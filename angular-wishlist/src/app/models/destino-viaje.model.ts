export class DestinoViaje {
	public selected: boolean;
	public descripcion: string[];
	constructor(public nom: String ,public  f: string, public ced: string, public cor:string, public tel:string, public dep:string, public votes: number=0){
		this.descripcion=['Agil','Creativo'];
	}
	setSelected(s: boolean) {
		this.selected = s;
	  }
	isSelected() {
		return this.selected;
	  }
	voteUp(){
		this.votes++;
	}
	voteDown(){
		this.votes--;
	}
	getNom(): String{
		return this.nom;
	}
}
