import{
    reducerDestinosViajes,
    DestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction,
    DestinosViajesActionTypes,
    intializeDestinosViajesState
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        //setup
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1','destino 2']);
        //action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        //assert
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nom).toEqual('destino 1');
    });
    it('should reduce new item added', ()=>{
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje("brandon XD","nada","123456","branditon@gmail.com","3008668751","antioquia"));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nom).toEqual('brandon XD');
    })
})