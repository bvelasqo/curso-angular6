import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import {trigger, state, style, transition, animate, animation } from '@angular/animations';
import { ListaDestinosComponent } from '../lista-destinos/lista-destinos.component';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css'],
  animations: [
    trigger('esFavorito',[
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'whitesmoke'
      })),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('3s')
      ]),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('1s')
      ])
    ])
  ]
})
export class DestinoviajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass='col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>; 

  constructor( private store: Store<AppState>) {
    this.clicked = new EventEmitter;
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino)
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
