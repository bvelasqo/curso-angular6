import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { inject } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { map, debounceTime, distinctUntilChanged, switchMap, filter } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
fg:FormGroup;
minLongitud=10;
searchResults: string[];
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded=new EventEmitter();
    this.fg=fb.group({
      nombre: ['',Validators.required],url: [''], ced: ['',Validators.compose([Validators.required,this.validator, this.nombrevalidatorparametrizable(this.minLongitud)])],cor: ['',Validators.required],tel: [''],dep: ['',Validators.required] 
    });
    this.fg.valueChanges.subscribe((form: any)=>{
      console.log("cambio en el formulario", form);
    })
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input').pipe(map((e: KeyboardEvent)=>(e.target as HTMLInputElement).value), filter(Text => Text.length > 2),
    debounceTime(120), distinctUntilChanged(),
    switchMap((text: String) => ajax(this.config.apiEndpoint + '/ciudades?q=' +text))
    ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response); 
  }

  guardar(nombre: string, url: string,ced: string,cor: string,tel: string,dep: string):boolean{
    let d=new DestinoViaje(nombre,url,ced,cor,tel,dep);
    this.onItemAdded.emit(d);
    return false;
  }
  validator(control: FormControl): { [s: string]: boolean }{
    const l = control.value.toString().trim().length;
    if(l > 0 && l < 10){
      return { minlong: true };
    }
    return null;
  }

  nombrevalidatorparametrizable(minlong: number): ValidatorFn{
    return(control: FormControl):{[s: string]:boolean}|null=> {
      const l = control.value.toString().trim().length; 
      if(l > 0 && l < minlong){
        return { minlongnombre: true };
      }
      return null;
    };
  }
}
