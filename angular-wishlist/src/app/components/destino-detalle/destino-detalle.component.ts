import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DestinoApiClient } from 'src/app/models/destinos-api-client.model';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinoApiClient]
})
export class DestinoDetalleComponent implements OnInit {
destino: DestinoViaje;
nom: String;
style = {
  sources: {
    world:{
      type: 'geojson',
      data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
    }
  },
  version: 8,
  layers:[{
    'id': 'countries',
    'type': 'fill',
    'source': 'world',
    'layout': {},
    'paint': {
      'fill-color' : '#6F788A'
    }
  }]
}
  constructor(private route: ActivatedRoute, private destinoApiClient: DestinoApiClient) {
    route.params.subscribe(params => { this.nom = params['id']; });
   }
  ngOnInit(): void {
    let id= this.route.snapshot.paramMap.get('id');
    this.destino = this.destinoApiClient.getById(id);
  }

}
