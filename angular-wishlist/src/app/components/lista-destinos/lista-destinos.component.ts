import { state } from '@angular/animations';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';
import { DestinoApiClient }from './../../models/destinos-api-client.model'
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinoApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  constructor(public destinosApiClient:DestinoApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates=[];
    this.store.select(state => state.destinos.favorito).subscribe(d => {
      if( d != null ) {
        this.updates.push('se ha elegido'+d.nom);
      }
    });
    this.all = store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  borrar(){
    this.destinosApiClient.pop()
  }
  elegido(d: DestinoViaje){
    this.destinosApiClient.elegir(d);
  }

  getAll(){

  }
}
