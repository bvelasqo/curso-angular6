import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaludosMorComponent } from './saludos-mor.component';

describe('SaludosMorComponent', () => {
  let component: SaludosMorComponent;
  let fixture: ComponentFixture<SaludosMorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaludosMorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaludosMorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
